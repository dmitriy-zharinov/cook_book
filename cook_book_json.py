import json

#читаем файл
def read_json_file(filename, encoding):
    with open (filename, encoding=encoding) as data_file:    
        data = json.load(data_file)
    return data

def get_shop_list_by_dishes(dishes, person_count, cook_book):
    shop_list = {}
    
    for recipe in cook_book:
        if recipe['recipe_name'] in dishes:
            for ingredient in recipe['ingredients']:
                print(ingredient)
                new_shop_list_item = dict(ingredient)
                new_shop_list_item['quantity'] *= person_count
                if new_shop_list_item['ingredient_name'] not in shop_list:
                    shop_list[new_shop_list_item['ingredient_name']] = new_shop_list_item
                else:
                    shop_list[new_shop_list_item['ingredient_name']]['quantity'] += new_shop_list_item['quantity']
    return shop_list


def print_shop_list(shop_list):
    for shop_list_item in shop_list.values():
        print('{ingredient_name} {quantity} {measures}'.format(**shop_list_item))


def main():
    person_count = int(input('Сколько человек?'))
    dishes = input('Введите блюда в расчете на человека (через запятую): ').lower().split(', ')
    cook_book = read_json_file('cookbook.json', 'utf-8')
    shop_list = get_shop_list_by_dishes(dishes, person_count, cook_book)
    print_shop_list(shop_list)

main()
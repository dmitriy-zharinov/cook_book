def read_from_file():
    cook_book_list = []
    with open('recipes.txt', mode='r', encoding='utf-8') as file:
        for line in file:   #читаем в список
            line = line.rstrip('\n').lower()
            if line:        #если строка не пустая
                cook_book_list.append(line)
    return cook_book_list


def form_cook_book_from_file():
    cook_book = {}
    cook_book_list = read_from_file()
    index = 0
    while index < len(cook_book_list) - 2:
        num_of_ingredients = int(cook_book_list[index + 1])
        ingredients_list = []
        i = 1

        while i <= num_of_ingredients:
            cook_book_item = {}
            line = cook_book_list[index + 1 + i]
            #разбиваем строку 
            cook_book_item['ingredient_name'], cook_book_item['quantity'], cook_book_item['measures'] = line.split(' | ')
            #если целое число, то преобразуем его в int
            cook_book_item['quantity'] = float(cook_book_item['quantity'])
            if cook_book_item['quantity'].is_integer():
                cook_book_item['quantity'] = int(cook_book_item['quantity'])

            ingredients_list.append(cook_book_item)
            i += 1
        
        cook_book[cook_book_list[index]] = ingredients_list
        index += i + 1
    return cook_book


def get_shop_list_by_dishes(dishes, person_count, cook_book):
    shop_list = {}
    for dish in dishes:
        
        for ingredient in cook_book[dish]:
            new_shop_list_item = dict(ingredient)
            new_shop_list_item['quantity'] *= person_count
            if new_shop_list_item['ingredient_name'] not in shop_list:
                shop_list[new_shop_list_item['ingredient_name']] = new_shop_list_item
            else:
                shop_list[new_shop_list_item['ingredient_name']]['quantity'] += new_shop_list_item['quantity']
    return shop_list


def print_shop_list(shop_list):
    for shop_list_item in shop_list.values():
        print('{ingredient_name} {quantity} {measures}'.format(**shop_list_item))


def create_shop_list(cook_book):
    person_count = int(input('Сколько человек?'))
    dishes = input('Введите блюда в расчете на человека (через запятую): ').lower().split(', ')
    shop_list = get_shop_list_by_dishes(dishes, person_count, cook_book)
    print_shop_list(shop_list)

create_shop_list(form_cook_book_from_file())